---
name: Contact us
---
# Contact us

## Registration

All questions and inquiries regarding registration should be directed to
the Registration Team: [registration@debconf.org][]

## Talks

Questions and inquiries about talks and paper submissions:
[content@debconf.org][]

## Sponsorship (fundraising)

Send your questions and inquiries about sponsorship and becoming a
sponsor: [sponsors@debconf.org][]

## Harassment and inappropriate behaviour

Please see the [Code of Conduct](../about/coc/) page for more information.

## Other contacts

These are the chat rooms to discuss about organising and participating in this
event.

- [Common Matrix room][]
- [DebConf India Matrix room][], bridged to [XMPP][]
- [#debconf-team][] on OFTC IRC network

You can also join the mailing lists to get more updates about the event.

- [debconf-discuss@lists.debian.org][]
- [debconf-announce@lists.debian.org][] 

[registration@debconf.org]: mailto:registration@debconf.org
[sponsors@debconf.org]: mailto:sponsors@debconf.org
[content@debconf.org]: mailto:content@debconf.org
[visa@debconf.org]: mailto:visa@debconf.org
[Common Matrix room]: https://matrix.to/#/#debconf:matrix.debian.social
[Debconf India Matrix room]: https://matrix.to/#/#debconfindia:poddery.com
[XMPP]: xmpp:debconfindia@groups.poddery.com?join
[#debconf-team]: https://webchat.oftc.net/?channels=debconf-team
[debconf-discuss@lists.debian.org]: https://lists.debian.org/debconf-discuss
[debconf-announce@lists.debian.org]: https://lists.debian.org/debconf-announce
