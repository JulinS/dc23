from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand

from wafer.talks.models import Talk


FROM = 'content@debconf.org'

SUBJECT = 'Please try our speaker test station before your talk'
BODY = '''\
Dear conference speaker,

The videoteam has a new toy this year, a test station!

The goal of this test station is to validate your laptop works with our
projector setup.

Before your talk (preferably at least 24h in advance), we kindly as you to drop
by the video team room (red building) with your laptop and follow the
instructions on the test station screen.

That way, if something does not work, we'll be able to help you out before your
talk :).

Cheers,

The DebConf Video Team
'''


class Command(BaseCommand):
    help = "Ask speakers to come and try the test station"

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),

    def badger(self, talk, dry_run):
        try:
            scheduleitem = talk.scheduleitem_set.get()
        except ObjectDoesNotExist:
            return

        to = [user.email for user in talk.authors.all()]

        if dry_run:
            print('I would badger speakers of: %s'
                  % talk.title.encode('utf-8'))
            return
        email_message = EmailMultiAlternatives(
            SUBJECT, BODY, from_email=FROM, to=to)
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        for talk in Talk.objects.all():
            self.badger(talk, dry_run)
